import de.undercouch.gradle.tasks.download.Download

plugins {
    java
    id("de.undercouch.download") version("3.4.3")
}

repositories {
    mavenCentral()
}

group = "it.unibo.as"
version = "1.0-SNAPSHOT"

subprojects {

    repositories {
        mavenCentral()
    }

    apply<JavaPlugin>()
    //apply<ApplicationPlugin>()

    dependencies {
        implementation("it.unibo.alice.tuprolog", "tuprolog", "3.3.0")
        testImplementation("junit", "junit", "4.12")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}

val tuprologDir = File("tuprolog")
val tuprologZip = File(tuprologDir, "2p.zip")
val tuprologJar = File(tuprologDir, "bin/2p.jar")

dependencies {
    implementation("it.unibo.alice.tuprolog", "tuprolog", "3.3.0")
}

task<Download>("download2p") {
    src("https://bitbucket.org/tuprologteam/tuprolog/downloads/2p-3.3.0.zip")
    dest(tuprologZip)
    onlyIfModified(true)
}

task<Copy>("unzip2p") {
    dependsOn("download2p")
    from(zipTree(tuprologZip))
    destinationDir = tuprologDir
}

task<JavaExec>("2p") {
    dependsOn("unzip2p")
    classpath = files(tuprologJar)
    main = "alice.tuprologx.ide.CUIConsole"
    standardInput = System.`in`

    if (properties.containsKey("theory")) {
        args = listOf(properties["theory"].toString())
    }

    doFirst {
        if (properties.containsKey("theory")) {
            println("Loading theory: ${properties["theory"]}")
        }
    }
}

task<JavaExec>("2p-gui") {
    dependsOn("unzip2p")
    classpath = files(tuprologJar)
    main = "alice.tuprologx.ide.GUILauncher"
    standardInput = System.`in`

}
