:- op(900, xfx, in).

preconditions(Action, PrecList) :-
    action(Action, 'if'(PrecList), _, _).

precondition(Action, Prec) :-
    preconditions(Action, PrecList),
    member(Prec, PrecList).


positive_effects(Action, AddList) :-
    action(Action, _, '+'(AddList), _).

positive_effect(Action, Effect) :-
    positive_effects(Action, AddList),
    member(Effect, AddList).


negative_effects(Action, RemoveList) :-
    action(Action, _, _, '-'(RemoveList)).

negative_effect(Action, Effect) :-
    negative_effects(Action, RemoveList),
    member(Effect, RemoveList).

% strips(+InitialState, +FinalGoal, -Plan)
strips(InitState, Goal, Plan) :-
    strips_impl(InitState, Goal, [], [], _, RevPlan),
    reverse(RevPlan, Plan).


% strips_impl(+State, +Goal, +InitialPlan, +BadActions, -NextState, -FinalPlan)
strips_impl(State, Goal, Plan, _, State, Plan) :- fail, % <-- TODO remove this
    /* Check if Goal is satisfied in State */
    write("Reached goal "), write(Goal), write(" in "), write(State),
    reverse(Plan, ActualPlan), % this is just for logging purposes
    write(" through plan "), write(ActualPlan), unindent, newline.

% strips_impl(+State, +Goal, +InitialPlan, +BadActions, -NextState, -FinalPlan)
strips_impl(State, Goal, Plan, BadActions, FinalState, FinalPlan) :- fail, % <-- TODO remove this
    write("Need to reach "), write(Goal), write(" from "), write(State), write(" without using "), write(BadActions), indent, newline,
    /* TODO select an unsatisfied SubGoal in Goal */
    write('Attempting goal:  '), write(SubGoal), newline,
    /* TODO select an Action which may produce SubGoal */
    write('Choosing Action:  '), write(Action), newline,
    /* TODO ensure the selected Action is not blacklisted */
    write("Action "), write(Action), write(" is not a bad action"), newline,
    /* TODO check if Action can be applied to the current state */
    /* TODO if not, find a SubPlan making Action applicable */
    /* TODO if such a SubPlan exists, let TmpState be the state reached by applying SubPlan to State */
    apply(TmpState, Action, NewState),
    strips_impl(NewState, Goal, [Action | SubPlan], BadActions, FinalState, FinalPlan).

% strips_impl(+State, +Goal, +InitialPlan, +BadActions, -NextState, -FinalPlan)
strips_impl(_, _, _, _, _, _) :- % this is here just for logging purposes
    unindent, !, fail.


% apply(+State, +Action, -NewState)
apply(State, Action, NewState) :-
    write('Simulating '), write(Action), newline,
    write('Transition: '), write(State),
    write(" ==> "),
    /* TODO, compute new state */
    write(NewState), newline.
