
:- op(150, fx, if).
:- op(200, fx, '+').
:- op(200, fx, '-').

action(
    stack(X, Y),
    if [clear(Y), holding(X)],
    + [on(X,Y), clear(X), handempty],
    - [clear(Y), holding(X)]
).

