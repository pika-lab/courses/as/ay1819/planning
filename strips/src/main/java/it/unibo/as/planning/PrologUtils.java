package it.unibo.as.logicagents;

import alice.tuprolog.*;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class PrologUtils {
    public static boolean assertA(Prolog engine, Term term) {
        return engine.solve(new Struct("asserta", term)).isSuccess();
    }

    public static boolean assertZ(Prolog engine, Term term) {
        return engine.solve(new Struct("assertz", term)).isSuccess();
    }

    public static boolean retract(Prolog engine, Term term) {
        return engine.solve(new Struct("retract", term)).isSuccess();
    }

    public static boolean retractAll(Prolog engine, Term term) {
        return engine.solve(new Struct("retractall", term)).isSuccess();
    }

    public static boolean update(Prolog engine, Struct term) {
        final Term[] placeholders = IntStream.range(0, term.getArity())
                .mapToObj(i -> new Var())
                .toArray(Term[]::new);

        return retract(engine, new Struct(term.getName(), placeholders))
                && assertA(engine, term);
    }

    public static boolean update(Prolog engine, Term term) {
        return update(engine, (Struct) term);
    }

    public static Struct couple(Term a, Term b) {
        return new Struct(",", a, b);
    }

    public static Stream<Term> theoryToStream(Prolog engine) {
        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        engine.getTheory().iterator(engine),
                        Spliterator.ORDERED
                ),
                false
        );
    }
}
