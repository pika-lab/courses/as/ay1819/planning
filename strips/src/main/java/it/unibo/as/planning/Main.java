package it.unibo.as.planning;

import alice.tuprolog.*;
import alice.tuprolog.event.ExceptionEvent;
import alice.tuprolog.event.OutputEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Stream;
import java.util.stream.IntStream;

import static it.unibo.as.logicagents.PrologUtils.assertA;

public class Main {

    private static final BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
    private static boolean verbose;

    private static String initialState = null;
    private static String goal = null;
    private static String worldFile = null;
    private static String maxDepth = "10";

    public static void main(String[] args) throws InvalidTheoryException, MalformedGoalException, IOException, NoMoreSolutionException {

        verbose = hasFlag(args, "-v", "--verbose");
        initialState = getArg(args, initialState, "-is", "--initialState");
        goal = getArg(args, goal, "-g", "--goal");
        worldFile = getArg(args, worldFile, "-w", "--world");
        maxDepth = getArg(args, maxDepth, "-md", "--maxDepth");

        InputStream worldStream = Main.class.getResourceAsStream("/" + worldFile + ".pl");
        Theory theory = worldStream != null ? new Theory(worldStream) : new Theory("");
        theory.append(new Theory(Main.class.getResourceAsStream("/Strips.pl")));
        theory.append(new Theory(Main.class.getResourceAsStream("/Utils.pl")));

        Prolog agent = createEngineWithTheory(theory);

        assertA(agent, new Struct("max_depth", new Int(Integer.parseInt(maxDepth))));

        for (String query = null; true; goal = null, initialState = null) {

            if (goal == null || initialState == null) {
                System.out.print("?- ");
                System.out.flush();
                query = stdIn.readLine();
            } else {
                query = String.format("strips(%s, %s, Plan).", initialState, goal);
                System.out.printf("?- %s\n", query);
                System.out.flush();
            }


            SolveInfo si = agent.solve(query);

            while (si != null) {

                System.out.println();
                System.out.println(si);
                System.out.flush();

                if (si.hasOpenAlternatives()) {
                    stdIn.readLine();
                    si = agent.solveNext();
                } else {
                    si = null;
                }
            }
        }
    }

    public static Prolog createEngineWithTheory(InputStream theory) throws InvalidTheoryException, IOException {
        return createEngineWithTheory(new Theory(theory));
    }

    public static Prolog createEngineWithTheory(String theory) throws InvalidTheoryException {
        return createEngineWithTheory(new Theory(theory));
    }

    public static Prolog createEngineWithTheory(Theory theory) throws InvalidTheoryException {
        Prolog engine = new Prolog();
        engine.setTheory(theory);
        if (verbose) {
            engine.addOutputListener(Main::printOutput);
            engine.addExceptionListener(Main::printError);
        }
        return engine;
    }

    private static void printOutput(OutputEvent object) {
        System.out.print(cleanUpMessage(object.getMsg()));
    }

    private static void printError(ExceptionEvent object) {
        System.err.print(cleanUpMessage(object.getMsg()));
    }

    private static String cleanUpMessage(String msg) {
        if (msg.startsWith("'") && msg.endsWith("'")) {
            return msg.substring(1, msg.length() - 1);
        }
        return msg;
    }

    private static boolean hasFlag(String[] args, String... flags) {
        return Stream.of(args).anyMatch(a -> Stream.of(flags).anyMatch(f -> a.equals(f)));
    }

    private static String getArg(String[] args, String defaultArg, String... keys) {
        return IntStream.range(0, args.length)
                .mapToObj(i -> new Object[] { i, args[i] })
                .filter(a -> Stream.of(keys).anyMatch(k -> a[1].equals(k)))
                .map(a -> args[((Integer) a[0]) + 1])
                .findAny()
                .orElse(defaultArg);
    }
}
